document.addEventListener("DOMContentLoaded", function() {

  const menu = document.querySelector('.navbar__menu-wrapper');
  const menuBtn = document.querySelector('.navbar__menu-toggle');

  menuBtn.addEventListener('click', function (){
    menu.classList.toggle('active');
  });
});